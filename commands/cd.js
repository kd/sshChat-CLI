async function changeDir(commandArgs, USERPWD) {
  let newDir;
  if (commandArgs.startsWith("/")) {
    newDir = commandArgs;
    console.log("Directory Changed to: " + newDir)
  } else {
    newDir = USERPWD + "/" + commandArgs;
    console.log("Directory Changed to: " + newDir)
  }
  return newDir;
}
  module.exports = { changeDir }
  
  