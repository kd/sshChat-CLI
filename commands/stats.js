// stats.js
const unirest = require("unirest");

async function getStats(key) {
  let requestData = await unirest
      .get('https://api.discord-linux.com/stats')
      .headers({
          'Accept': 'application/json', 'Content-Type': 'application/json',
          'x-discord-linux-auth': key
      })
  return requestData.body
}

async function stats(key) {
  const response = await getStats(key)
  console.log(response)
}

module.exports = { stats, getStats }