// start.js
const unirest = require("unirest");

async function startContainer(key) {
    let startContainer = await unirest
        .get('https://api.discord-linux.com/start')
        .headers({
            'Accept': 'application/json', 'Content-Type': 'application/json',
            'x-discord-linux-auth': key
        })
    return startContainer.body
}

async function start(key) {
    const response = await startContainer(key)
    console.log(response)
}

module.exports = { start, startContainer }
