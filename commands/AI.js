// AI.js
var unirest = require('unirest');

async function AIRequest(prompt) {
  console.log("Please wait while the AI thinks about this....\n")
  let AIRequest = await unirest
    .post('https://codex-ai-v9q6.onrender.com/')
    .headers({ 'Accept': 'application/json', 'Content-Type': 'application/json' })
    .send({ "prompt": prompt })
    console.log(AIRequest.body.bot)
  return AIRequest.body.bot
}

  module.exports = { AIRequest }