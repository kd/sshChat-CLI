// exec.js
const unirest = require("unirest");

async function runCMD(key, cmd, pwd) {
  let requestData = await unirest
    .post('https://api.discord-linux.com/exec')
    .headers({
      'Accept': 'application/json', 'Content-Type': 'application/json',
      'x-discord-linux-auth': key
    })
    .send({ "cmd": cmd, "pwd": pwd })
  return requestData.body.stdout
}

async function execute(key, cmd, pwd, conns, USERNAME) {
  const stdout = await runCMD(key, cmd, pwd)
  console.log(stdout)
  if (stdout.length > 1800) return console.log("I cannot send stdout to the swarm, its too large!")
  for (const conn of conns)
  conn.write(`${USERNAME}: ran ${cmd}\n${stdout}`)

}


module.exports = { execute, runCMD }